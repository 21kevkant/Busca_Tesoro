import javax.swing.*;
import java.awt.event.*;
import java.util.*;

public class BuscaT implements ActionListener
{
   private JFrame ventana;
   private JButton btn_boton, btn_iniciar, btn_tmp;
   private JLabel lbl_xNum,lbl_yNum, lbl_norte, lbl_sur, lbl_este, lbl_oeste, lbl_presentacion, lbl_direc, lbl_intent;
   private JTextField tf_direc, tf_intent;
   private Random rnd;
   private int tesoroX,tesoroY;
   
   
   public static void main(String[] args)
   {
      new BuscaT();

   }
   
   BuscaT()
   {
      
      ventana = new JFrame("Busca Tesoro");
      //el primero es el X, luego la Y. despues está el Ancho y Alto
      ventana.setBounds(100,100,500,500);
      ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      ventana.setExtendedState(JFrame.MAXIMIZED_BOTH);
      ventana.setLayout(null);
    
     rnd = new Random();

//con lo siguiente se estará creando las coordenas en la cual el tesoro estará oculto.
      int tesoro;
      tesoro = rnd.nextInt(99);
      tesoroX = 120+50*(tesoro%10);
      tesoroY = 75+20*(tesoro/10);
      
      //los siguientes for sirviran para crear 100 botones, la enumeracion de la X y Y respectivamente, el boton inicar.
      for (int i=0; i<100; i++)
      {
         btn_boton = new JButton();
         //posición X, Posicion Y, ancho,alto
         btn_boton.setBounds(120+50*(i%10),75+20*(i/10),50,20);
         btn_boton.addActionListener(this);
         ventana.add(btn_boton);
      }  
      
      int h=10;
      for (int n=1; n<=10; n++)
      {
         lbl_xNum = new JLabel(String.valueOf(n));
         lbl_xNum.setBounds(85+50*n,275,50,20);
         ventana.add(lbl_xNum);
         lbl_yNum = new JLabel(String.valueOf(h));
         lbl_yNum.setBounds(105,55+20*n,50,20);
         ventana.add(lbl_yNum);
         h--;
      }     
      
       btn_iniciar = new JButton("INICIAR JUEGO");
       btn_iniciar.setBounds(310,400,150,20);
       btn_iniciar.addActionListener(this);
       ventana.add(btn_iniciar);
      //fin de los botones
      
      //En los lbl van las etiquetas que están al rededor de los botones, y ellos serviran para guíar al usuario.
      lbl_norte = new JLabel(" NORTE");
      lbl_norte.setBounds(350,25,50,30);
      ventana.add(lbl_norte);
      
      lbl_sur = new JLabel(" SUR");
      lbl_sur.setBounds(350,300,50,30);
      ventana.add(lbl_sur);
      
      lbl_oeste = new JLabel(" OESTE");
      lbl_oeste.setBounds(50,175,80,20);
      ventana.add(lbl_oeste);
      
      lbl_este = new JLabel(" ESTE");
      lbl_este.setBounds(650,175,80,20);
      ventana.add(lbl_este);
      
      lbl_direc = new JLabel("VIAJE A LA DIRECCION: ");
      lbl_direc.setBounds(180,470,150,20);
      ventana.add(lbl_direc);
      
      lbl_intent = new JLabel("INTENTOS: ");
      lbl_intent.setBounds(475,470,125,20);
      ventana.add(lbl_intent);
      //fin de las etiquetas.
      
      //En las siguientes lineas de código van todos los textField que el juego necesitaria para guiar al usuario.      
      tf_direc = new JTextField();
      tf_direc.setBounds(290,500,85,20);
      ventana.add(tf_direc);
      
      tf_intent = new JTextField("0");
      tf_intent.setBounds(520,500,30,20);
      ventana.add(tf_intent);
      
      ventana.setVisible(true);
   }
   
   public void actionPerformed(ActionEvent e)
   {   
   
      int x, y;
      int v;
      v = Integer.parseInt(tf_intent.getText());
      tf_intent.setText(String.valueOf(v+1));
      
      btn_tmp = (JButton)e.getSource();
        
      x = btn_tmp.getLocation().x;
      y = btn_tmp.getLocation().y;

      if (e.getSource() == btn_iniciar)
        {
           tf_intent.setText(String.valueOf(0));
           tf_direc = new JTextField(" ");
           tf_direc.setBounds(290,500,85,20);
           ventana.add(tf_direc);
           //ventana.removeAll();
           int tesoro;
           tesoro = rnd.nextInt(99);
           tesoroX = 120+50*(tesoro%10);
           tesoroY = 75+20*(tesoro/10);
      
        } 
      else 
        {
      //las siguientes lineas muestran las coordenadas que el usuario debe seguir para encontrar el tesoro.
       if (x < tesoroX && y > tesoroY)
            tf_direc = new JTextField("NORESTE");
            tf_direc.setBounds(290,500,85,20);
            ventana.add(tf_direc);
            
        if (x > tesoroX && y > tesoroY)
            tf_direc = new JTextField("NOROESTE");
            tf_direc.setBounds(290,500,85,20);
            ventana.add(tf_direc);
            
        if (x < tesoroX && y < tesoroY)
            tf_direc = new JTextField("SURESTE");
            tf_direc.setBounds(290,500,85,20);
            ventana.add(tf_direc);
            
        if (x > tesoroX && y < tesoroY)
            tf_direc = new JTextField(" SUROESTE");
            tf_direc.setBounds(290,500,85,20);
            ventana.add(tf_direc);
            
        if (x > tesoroX && y == tesoroY)
            tf_direc = new JTextField("OESTE");
            tf_direc.setBounds(290,500,85,20);
            ventana.add(tf_direc);
            
        if (x < tesoroX && y == tesoroY)
            tf_direc = new JTextField("ESTE");
            tf_direc.setBounds(290,500,85,20);
            ventana.add(tf_direc);
            
        if (x == tesoroX && y < tesoroY)
            tf_direc = new JTextField("SUR");
            tf_direc.setBounds(290,500,85,20);
            ventana.add(tf_direc);
            
        if (x == tesoroX && y > tesoroY)
            tf_direc = new JTextField("NORTE");
            tf_direc.setBounds(290,500,85,20);
            ventana.add(tf_direc);
//fin de las coordenadas para buscar el tesoro.
         }
        if (x == tesoroX & y == tesoroY)
                 JOptionPane.showMessageDialog(null, "FELICIDADES!  ENCONTRASTE EL TESORO"); 
            //JTextField.removeAll();
            //remove(JLabel);
      
   }
}